# My Scripts

## [Web browser homepage](https://gitlab.com/IM-OUT-OF-ORDER/scripts/-/tree/master/homepage)
- My custom homepage complete with multiple themes

## [Keybindings](https://gitlab.com/IM-OUT-OF-ORDER/scripts/-/tree/master/keybindings)
- My easy-to-use keybindings for Linux with a help GUI

## [Colorscripts](https://gitlab.com/IM-OUT-OF-ORDER/scripts/-/tree/master/shell-color-scripts)
- Random color scripts in terminal (Here you can find the [Original versions](https://gitlab.com/dwt1/shell-color-scripts))

## Bash Insulter
- Bash insults

## Conky Chooser
- Easy way to change conky theme

## ASF (ArchiSteamFarm)
- More advanced version of Idle Master

## Game Idle
- Meant to be used with Idle Master for Steam (makes it easier to choose the games)

## Setup
- Downloads and installs everything for a fresh Arch-based install
