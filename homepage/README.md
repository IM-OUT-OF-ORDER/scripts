# My Web Browser Homepages

## Desktop (have not been able to do much work on, lately...plans are to make it more like laptop version)
- Made for higher resolution devices
- Includes page icon and images for a more unique experience
- Black and white containers
- Links and searches open a new tab
- Displays a welcome message, time, weather, 6 popular site categories and a dedicated search page

## Laptop
- Made specifically for lower resolution devices
- Includes page icon
- Multiple themes with unique glowing containers
- Links and searches open in the same tab
- Displays a welcome message, search bar, date, time, weather, and 6 popular site categories

## How to use
- Place in whatever directory you would like
- Make sure to add the path to your web browser (you can add it as the home button also)
- Add a tab redirect extension to launch homepage with new tab

#### Change weather location
- https://openweathermap.org/
- Search for city
- Copy the numbers that appear in the search bar after "city"
- Open "homepage.html" in your text editor
- Look for "Request to open weather map"
- In the following line change the numbers that appear after "weather?id=" to the ones that appeared in the search bar
- Save the file and then refresh the homepage in your web browser

#### Change theme
- For Linux run .hcc in terminal
- Windows theme change method coming soon (for now manually change theme name on line 123)
